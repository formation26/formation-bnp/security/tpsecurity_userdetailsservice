package com.example.TPSecurityUserService.application;

import com.example.TPSecurityUserService.domaine.Client;

public interface IClientService {

    Client findByEmail(String email);
}
