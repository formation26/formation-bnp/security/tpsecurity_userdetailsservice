package com.example.TPSecurityUserService.repository;

import com.example.TPSecurityUserService.domaine.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientRepository extends CrudRepository<Client,String> {


    Client findByEmail(String email);
}
