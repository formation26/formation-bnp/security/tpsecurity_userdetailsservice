package com.example.TPSecurityUserService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpSecurityUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpSecurityUserServiceApplication.class, args);
	}

}
