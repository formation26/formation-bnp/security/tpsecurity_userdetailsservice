package com.example.TPSecurityUserService.exposition;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {

    @GetMapping("/getHello")
    public String sayHello(){
        return "hello ";
    }

    @GetMapping("/admin/getAdminHello")
    public String sayHelloAdmin(){
        return "hello admin!!";
    }
}
